use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use lambda_runtime::{Error as LambdaError, LambdaEvent, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use simple_logger::SimpleLogger;

#[derive(Deserialize)]
struct Request {
    user_id: String,
}

#[derive(Serialize)]
struct Response {
    username: String,
    netid: String,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    SimpleLogger::new().with_utc_timestamps().init()?;
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Request>) -> Result<Value, LambdaError> {
    let request = event.payload;
    let config = load_from_env().await;
    let client = Client::new(&config);

    let response = get_user_info(&client, request.user_id).await?;

    Ok(json!(response))
}

async fn get_user_info(client: &Client, user_id: String) -> Result<Response, LambdaError> {
    let table_name = "Users";

    let resp = client.get_item()
        .table_name(table_name)
        .key("user_id", AttributeValue::S(user_id))
        .send()
        .await?;

    if let Some(item) = resp.item() {
         // Assuming item contains username and netid fields
         let username: String = item.get("username").and_then(|v| v.as_s().ok()).and_then(|n| n.parse().ok()).unwrap_or_default();
         let netid = item.get("netid").and_then(|v| v.as_s().ok()).and_then(|n| n.parse().ok()).unwrap_or_default();
        
        Ok(Response { username, netid })
        
    } else {
        Err(LambdaError::from("No user found for the provided ID"))
    }
}
