## Overview
This project is a Rust-based serverless microservice deployed on AWS Lambda. It interacts with DynamoDB to retrieve user information based on a provided user ID.

## Functionality
The service exposes an API endpoint that accepts POST requests with a JSON payload containing a `user_id`. It retrieves the corresponding user information from a DynamoDB table and returns it in the response.

## Deployment
The service is deployed on AWS Lambda using the `lambda_runtime` crate for handling Lambda events. It is triggered by API Gateway with an HTTP endpoint.

## Prerequisites
- Rust (and Cargo)
- AWS account with appropriate permissions
- DynamoDB table set up with the required schema

## Installation
1. Clone the repository:
```bash
git clone https://gitlab.com/dukeaiml/IDS721/week5.git
cd your_repository
```

2. Set up AWS credentials:
export AWS_ACCESS_KEY_ID=your_access_key_id
export AWS_SECRET_ACCESS_KEY=your_secret_access_key

3. Compile and deploy the service:
cargo build --release
cargo deploy

4. Usage
Send a POST request to the API endpoint with a JSON payload containing the user_id:
```
curl -X POST -H "Content-Type: application/json" -d '{"user_id":"your_user_id_here"}' https://b77oqkyvqd.execute-api.us-east-1.amazonaws.com/test/week5
```

## Environment Variables
* AWS_ACCESS_KEY_ID: AWS access key ID
* AWS_SECRET_ACCESS_KEY: AWS secret access key

## Dependencies

* aws_sdk_dynamodb: Rust SDK for DynamoDB interaction
* lambda_runtime: Rust crate for handling Lambda events
* serde: Serialization and deserialization for JSON
* serde_json: JSON support for Serde
* simple_logger: Lightweight logging utility for Rust

## Screenshots
* Dyanmo DB
![](images/db.png)
* Lambda function response
![](images/test.png)
